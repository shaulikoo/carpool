const aws = require('aws-sdk');
//endpoint: 'https://dynamodb.us-east-1.amazonaws.com'
//http://interview-VirtualBox:8000

aws.config.update({
    region: 'us-east-1',
    endpoint: 'http://localhost:8000'
});

const dynamodb = new aws.DynamoDB()

var params = 
{
    AttributeDefinitions: [
        {
            AttributeName: "id", 
            AttributeType: "S"
        }
    ], 
    KeySchema: [
        {
            AttributeName: "id", 
            KeyType: "HASH"
        }
    ], 
    ProvisionedThroughput: {
     ReadCapacityUnits: 5, 
     WriteCapacityUnits: 5
    }, 
    TableName: "Tremp"
};



dynamodb.createTable(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);  
});
