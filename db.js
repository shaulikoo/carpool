const aws = require('aws-sdk');
//endpoint: 'https://dynamodb.us-east-1.amazonaws.com'
//endpoint: 'http://interview-VirtualBox:8000'
aws.config.update({
    region: 'us-east-1',
    endpoint: 'http://localhost:8000'
});

const docClient = new aws.DynamoDB.DocumentClient();

module.exports = docClient;
