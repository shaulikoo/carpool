const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const uuid = require("uuid/v4");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const tokenService = require('../Auth/tokkens');

const docClient = require("../db");
let table = "Tremp";
// CREATES A NEW USER
router.post('/',  tokenService.VerifyToken ,function (req, res) {
    var params = {
        TableName:table,
        Item:{
            "driver":{
                "email":req.body.driver.email,
                "name":req.body.driver.name,
                "phone":req.body.driver.phone
            },
            "date":req.body.date,
            "fromLocation":req.body.fromLocation,
            "toLocation":req.body.toLocation,
            "maxPassengers":req.body.maxPassengers,
            "id": uuid()   
        }
    };
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            res.status(401).json({success:false, message: err});
        } else {
            res.status(200).json({success:true});
        }
    });
});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', tokenService.VerifyToken ,function (req, res) {
    docClient.scan({TableName:table},function(err,data){
        if (err) return res.status(500).json({success:false, message:"There was a problem finding the tremp."});
        res.status(200).json({success:true, data:data});
    });
});


module.exports = router;