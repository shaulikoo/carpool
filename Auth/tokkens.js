var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

class TokenService{
    
    GenerateToken(params){
        let lifetime = params.lifetime * 60 * 60;
        var token = jwt.sign({ "email": params.email, "phone":params.phone }, process.env.se || 'siemensSec', {
            expiresIn: lifetime // expires in 24 hours
        });
        return token;
    }

    VerifyToken(req,res,next){
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if(token){
            jwt.verify(token, process.env.se || 'siemensSec', (err, decoded) => {
                if (err) {
                    return res.status(401).json({ success: false, message: 'Failed to authenticate token.' });
		        }
                req.decoded = decoded;
                next();
            });
        }
        else
        {
            return res.status(401).json({ success: false, message: 'Failed to authenticate token.' });
        }
    }
}

module.exports = new TokenService()
