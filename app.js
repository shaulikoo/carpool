var fs = require('fs');

var express = require('express');
var app = express();
var cors = require('cors');
var UserController = require('./User/UserController');
var Tremps = require('./Tremps/tremps');
app.use(cors());
app.use('/api/users', UserController);
app.use('/api/tremps',Tremps);


process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err);
    fs.writeFile("/home/carpool/error", err, function(err) {
        if(err) {
            return console.log(err);
        }
    });
});

module.exports = app;
