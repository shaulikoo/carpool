const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const uuid = require("uuid/v4");
const bcrypt = require("bcryptjs");

const tokenService = require('../Auth/tokkens');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

const docClient = require("../db");
let table = "Users";
// CREATES A NEW USER
router.post('/', function (req, res) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    var params = {
        TableName:table,
        Item:{
            "email": req.body.email,
            "name": req.body.name,
            "phone": req.body.phone,
            "area": req.body.area,
            "address":req.body.address,
            "role":req.body.role,
            "password":hashedPassword,
            "id": uuid()
        }
    };
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            res.status(401).send({"success": false,"error": err});
        } else {
            console.log("Added item:", data);
            let token = tokenService.GenerateToken({email: req.body.email, phone: req.body.phone, lifetime: 24 });
            res.status(200).send({"success": true, "token": token});
        }
    });
});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', tokenService.VerifyToken ,function (req, res) {
    docClient.scan({TableName:table},function(err,data){
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).json({success:true, data:data});
    });
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:email', tokenService.VerifyToken , function (req, res) {
    var params = {
        TableName: table,
        Key:{
            "email": req.params.email
        }
    };
    docClient.get(params, function(err, user) {
        if (err) return res.status(500).json({success:false, message:"There was a problem finding the user."});
        if (!user || !user.Item) return res.status(404).json({success:false, message:"No user found."});
        delete user.Item.password;
        res.status(200).json({success:true, data: user});
    });
});

// DELETES A USER FROM THE DATABASE
router.post('/delete', tokenService.VerifyToken , function (req, res) {
    var params = {
        TableName: table,
        Key:{
            "email": req.body.email
        }
    };
    docClient.delete(params, function(err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: "+ user.name +" was deleted.");
    });
});

// UPDATES A SINGLE USER IN THE DATABASE
router.post('/update', tokenService.VerifyToken , function (req, res) {
    var params = {
        TableName:table,
        Key:{
            "email": req.body.email
        },
        UpdateExpression: "set name = :n, phone = :p, area = :a, adress = :d, role = :r",
        ExpressionAttributeValues:{
            ":n":req.body.name,
            ":p":req.body.phone,
            ":a":req.body.area,
            ":d":req.body.address,
            ":r":req.body.role
        },
        ReturnValues:"UPDATED_NEW"
    };

    docClient.update(params, function(err, user) {
        if (err) {
            return res.status(500).send("There was a problem updating the user.");
        }
        else
        {
            return res.status(200).send(user);
        }
    });
});

router.post('/verifyCredentials', function(req,res){
    var params = {
        TableName: table,
        Key:{
            "email": req.body.email
        }
    };
    docClient.get(params, function(err, user) {
        if (err) return res.status(500).json({ success: false, message: "There was a problem finding the user."});
        if (!user) return res.status(404).json({ success: false, message: "No user found"});
        
        var passwordIsValid = bcrypt.compareSync(req.body.password, user.Item.password);
        if (!passwordIsValid) return res.status(401).json({ success: false, message: "Password error"});
        
        let token = tokenService.GenerateToken({email: req.body.email, phone: req.body.phone, lifetime: 24 });
        return res.status(200).json({ success: true, "token": token});
    });
});


module.exports = router;
